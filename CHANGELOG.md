# Changelog

## Unreleased


### Added

- Installable en tant que package Composer

### Changed

- spip/spip#4657 Mettre tout ce qui est administration des statistiques sur une page à part accessible via le menu maintenance
- Compatible SPIP 5.0.0-dev
